#include <stdio.h>

#define RADIUS 10
#define PI 3.14159

int main(void)
{
    printf("volume: %f\n", (4.0f / 3.0f) * PI * RADIUS * RADIUS * RADIUS);
}
