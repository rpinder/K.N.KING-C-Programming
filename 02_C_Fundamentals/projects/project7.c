#include <stdio.h>

int main(void)
{
    int amount;
    printf("Enter a dollar amount: ");
    scanf("%d", &amount);

    int num_of_twenties = amount / 20;
    printf("$20 bills: %d\n", num_of_twenties);
    amount -= 20 * num_of_twenties;

    int num_of_tens = amount / 10;
    printf("$10 bills: %d\n", num_of_tens);
    amount -= 10 * num_of_tens;

    int num_of_fives = amount / 5;
    printf(" $5 bills: %d\n", num_of_fives);
    amount -= 5 * num_of_fives;

    printf(" $1 bills: %d\n", amount);

    return 0;
}
