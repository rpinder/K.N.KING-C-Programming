#include <stdio.h>

int main(void)
{
    printf("%f\n", 8.0f / 5.0f);
    printf("%f\n", -8.0f / 5.0f);
    printf("%f\n", 8.0f / -5.0f);
    printf("%f\n", -8.0f / -5.0f);

    return 0;
}
