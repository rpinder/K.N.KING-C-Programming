#include <stdio.h>

int num_digits(int n);

int main(void) {
    printf("Number of digits in 109428 is %d\n", num_digits(109428));
    printf("Number of digits in 123456789 is %d\n", num_digits(123456789));

    return 0;
}

int num_digits(int n) {
    int counter = 0;
    while (n != 0) {
        n /= 10;
        counter++;
    }

    return counter;
}
