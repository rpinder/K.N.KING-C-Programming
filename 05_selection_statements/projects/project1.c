/* Write a program that calculates how many digits a number contains */

#include <stdio.h>

int main(void)
{
  int number, digits;

  printf("Enter a number: ");
  scanf("%d", &number);

  if (number < 9)
    digits = 1;
  else if (number < 99)
    digits = 2;
  else if (number < 999)
    digits = 3;
  else
    digits = 4;

  printf("The number %d has %d digits\n", number, digits);
  
  return 0;
}
