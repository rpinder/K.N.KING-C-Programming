#include <stdio.h>
#include <ctype.h>
#include <stdbool.h>

int main(void)
{
    int letter_occurance[26] = {0}, i;
    bool anagram = true;
    char ch;
    
    printf("Enter first word: ");
    for (i = 0; (ch = getchar()) != '\n'; i++) {
        if (isalpha(ch)) {
            letter_occurance[toupper(ch) - 'A']++;
        }
    }

    printf("Enter second word: ");
    for (i = 0; (ch = getchar()) != '\n'; i++) {
        if (isalpha(ch)) {
            letter_occurance[toupper(ch) - 'A']--;
        }
    }

    for (i = 0; i < 26; i++) {
        if (letter_occurance[i]) {
            anagram = false;
        }
    }

    if (anagram)
        printf("The words are anagrams.\n");
    else
        printf("The words are not anagrams.\n");

    return 0;
}
