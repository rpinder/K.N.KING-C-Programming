#include <stdio.h>

int main(void)
{
    int age = 16;
    int teenager = age >= 13 && age <= 19;
    printf("%d\n", teenager);

    age = 12;
    teenager = age >= 13 && age <= 19;
    printf("%d\n", teenager);

    age = 23;
    teenager = age >= 13 && age <= 19;
    printf("%d\n", teenager);

    return 0;
}
