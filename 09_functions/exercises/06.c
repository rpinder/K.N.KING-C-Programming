#include <stdio.h>
#include <math.h>

int digit(int n, int k);

int main(void)
{
    printf("digit(829, 1): %d\n", digit(829, 1));
    printf("digit(1503, 3): %d\n", digit(1503, 3));
    printf("digit(142, 4): %d\n", digit(142, 4));

    return 0;
}

int digit(int n, int k)
{
    int digit;

    while (k > 0) {
        digit = n % 10;
        n /= 10;
        k--;
    }
    return digit;
}
