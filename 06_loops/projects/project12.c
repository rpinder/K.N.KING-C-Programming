#include <stdio.h>

int factorial(int num) {
  if (num == 0 || num == 1) {
    return 1;
  } else {
    return (num * factorial(num - 1));
  }
}

int main(void)
{
  int i, n;
  float e = 1.0f, term_limit, next_term;
  
  printf("Enter number of loops: ");
  scanf("%d", &n);

  printf("Enter term limit: ");
  scanf("%f", &term_limit);

  for (i = 1; i < n; i++) {
    next_term = 1.0f/factorial(i); 
    if (next_term < term_limit)
      break;

    e += next_term;
  }

  printf("%f\n", e);
  
  return 0;
}
