#include <stdio.h>
#include <ctype.h>
#include <stdbool.h>

void read_word(int counts[26]);
bool equal_array(int counts1[26], int counts2[26]);

int main(void)
{
    int count_one[26] = {0}, count_two[26] = {0};
    bool anagram;
    
    printf("Enter first word: ");
    read_word(count_one);

    printf("Enter second word: ");
    read_word(count_two);

    anagram = equal_array(count_one, count_two);
    
    if (anagram)
        printf("The words are anagrams.\n");
    else
        printf("The words are not anagrams.\n");

    return 0;
}

void read_word(int counts[26])
{
    int i;
    char ch;
    for (i = 0; (ch = getchar()) != '\n'; i++) {
        if (isalpha(ch)) {
            counts[toupper(ch) - 'A']++;
        }
    }
}

bool equal_array(int counts1[26], int counts2[26])
{
    int i;
    
    for (i = 0; i < 26; i++) {
        if (counts1[i] != counts2[i])
            return false;
    }

    return true;
}
