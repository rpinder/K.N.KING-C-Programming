/* Checks numbers for repeated digits */

#include <stdbool.h>
#include <stdio.h>

int main(void)
{
  bool digit_seen[10] = {false};
  bool digit_repeated[10] = {false};
  bool is_repeated;
  int digit;
  long n;

  printf("Enter a number: ");
  scanf("%ld", &n);

  while (n > 0) {
    digit = n % 10;
    if (digit_seen[digit]) {
      digit_repeated[digit] = true;
      is_repeated = true;
    }
    digit_seen[digit] = true;
    n /= 10;
  }

  if (is_repeated) {
    printf("Repeated digit(s): ");

    for (size_t i = 0; i < sizeof(digit_repeated) / sizeof(digit_repeated[0]); i++) {
      if (digit_repeated[i])
        printf("%zu ", i);
    }
    printf("\n");
  } else {
    printf("No repeated digit(s)\n");
  }

  return 0;
}
