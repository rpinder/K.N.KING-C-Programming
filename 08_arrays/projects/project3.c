/* Checks numbers for repeated digits */

#include <stdbool.h>
#include <stdio.h>

int main(void)
{
    bool digit_seen[10] = {false}, repeated = false;
    int digit;
    long n, m;

    for (;;) {
        printf("Enter a number: ");
        scanf("%ld", &n);

        if (n<=0)
            break;

        while (n > 0) {
            digit = n % 10;
            if (digit_seen[digit]) {
                repeated = true;
                break;
            }
            digit_seen[digit] = true;
            n /= 10;
        }
    }

    if (repeated)
        printf("Repeated digit\n");
    else
        printf("No repeated digit\n");


    return 0;
}
