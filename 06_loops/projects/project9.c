#include <stdio.h>

int main(void)
{
  float amount_of_loan, interest, payment;
  int num_of_payments, i;

  printf("Enter amount of loan: ");
  scanf("%f", &amount_of_loan);

  printf("Enter interest rate: ");
  scanf("%f", &interest);
  interest = (interest / 12) / 100;

  printf("Enter monthly payment: ");
  scanf("%f", &payment);

  printf("Enter number of payments: ");
  scanf("%d", &num_of_payments);

  printf("\n");
  for (i = 0; i < num_of_payments; i++) {
    amount_of_loan = amount_of_loan - payment + (amount_of_loan * interest);
    printf("Balance remaining after payment %d: %.2f\n", i, amount_of_loan);
  }

  return 0;
}
