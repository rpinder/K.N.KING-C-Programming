#include <stdio.h>

int gcd(int m, int n);

int main(void)
{
    int m, n;

    m = 50;
    n = 75;

    printf("%d and %d: %d\n", m, n, gcd(m, n));

    return 0;
}

int gcd(int m, int n) {
    int rem;

    while (n != 0) {
        rem = m % n;
        m = n;
        n = rem;
    }

    return m;
}
