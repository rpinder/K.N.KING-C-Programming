#include <stdio.h>
#include <ctype.h>

int main(void)
{
    int shift_amount, i, j;
    char pre_encrypted_message[64] = "", post_encrypted_message[64] = "", ch;

    printf("Enter message to be encrypted: ");
    
    for (i = 0; (ch = getchar()) != '\n'; i++) {
        pre_encrypted_message[i] = ch;
    }

    printf("Enter shift amount (1-25): ");
    scanf("%d", &shift_amount);
    
    printf("Encrypted message: ");
    for (j = 0; j < i; j++) {
        ch = pre_encrypted_message[j];

        if (isalpha(ch)) {
            if (ch == toupper(ch)) {
                post_encrypted_message[j] = ((ch - 'A') + shift_amount) % 26 + 'A';
            } else if (ch == tolower(ch)) {
                post_encrypted_message[j] = ((ch - 'a') + shift_amount) % 26 + 'a';
            }
        } else {
            post_encrypted_message[j] = ch;
        }

        putchar(post_encrypted_message[j]);
     }

    return 0;
}
