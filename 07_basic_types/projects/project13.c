#include <stdio.h>

int main(void)
{
  int char_count = 0, word_count = 1;
  char ch;

  printf("Enter a sentence: ");

  while ((ch = getchar()) != '\n') {
    if (ch == ' ') {
      word_count++;
      continue;
    }
    char_count++;
  } 
  printf("Average word length: %.1f\n", (double)char_count / (double)word_count);
  
  return 0;
}
