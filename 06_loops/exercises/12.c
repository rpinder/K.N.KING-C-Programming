#include <stdio.h>

int main(void)
{
  int d, n = 63;

  for (d = 2; d * d < n; d++)
    if (n % d == 0) {
      printf("not prime\n");
      break;
    }

  return 0;
}
