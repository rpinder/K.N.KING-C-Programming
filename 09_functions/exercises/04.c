#include <stdio.h>

int day_of_year(int month, int day, int year);

int main(void)
{
    printf("%d\n", day_of_year(6, 27, 2014));
    
    return 0;
}

int day_of_year(int month, int day, int year) {
    int no_of_days = day;

    for (int i = 1; i < month; i++) {
        switch(i) {
        case 1:
        case 3:
        case 5:
        case 7:
        case 8:
        case 10:
        case 12:
            no_of_days += 31;
            break;
        case 2:
            no_of_days += (year % 4 == 0 && year % 100 != 0) || year % 400 == 0 ? 29 : 28;
            break;
        default:
            no_of_days += 30;
            break;
        }
    }

    return no_of_days;
}
