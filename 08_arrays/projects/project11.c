#include <stdio.h>
#include <ctype.h>

int main(void)
{
    char number[15], ch;
  
    printf("Enter phone number: ");

    for (int i = 0; (ch = getchar()) != '\n'; i++) {
        number[i] = ch;
    }

    printf("In numberic form: ");

    for (size_t i = 0; i < sizeof(number) / sizeof(number[0]); i++) {
        ch = toupper(number[i]);
        switch(ch) {
        case 'A':
        case 'B':
        case 'C':
            ch = '2';
            break;
        case 'D':
        case 'E':
        case 'F':
            ch = '3';
            break;
        case 'G':
        case 'H':
        case 'I':
            ch = '4';
            break;
        case 'J':
        case 'K':
        case 'L':
            ch = '5';
            break;
        case 'M':
        case 'N':
        case 'O':
            ch = '6';
            break;
        case 'P':
        case 'R':
        case 'S':
            ch = '7';
            break;
        case 'T':
        case 'U':
        case 'V':
            ch = '8';
            break;
        case 'W':
        case 'X':
        case 'Y':
            ch = '9';
            break;
        }

        printf("%c", ch);
    }
    printf("\n");
  
    return 0;
}

