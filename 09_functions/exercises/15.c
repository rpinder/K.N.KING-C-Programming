#include <stdio.h>

double median(double x, double y, double z);

int main(void)
{
    printf("Median of 1, 5, 2: %lf\n", median(1.00, 5.00, 2.00));
    
    return 0;
}

double median(double x, double y, double z) {
    double median = z;

    if (x <= y)
        if (y <= z) median = y;
        else if (x <= z) median = z;
        else median = x;
    if (z <= y) median = y;
    if (x <= z) median = x;

    return median;
}
