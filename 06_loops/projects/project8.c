#include <stdio.h>

int main(void)
{
  int num_days, start_day, i;

  printf("Enter number of days in month: ");
  scanf("%d", &num_days);

  printf("Enter starting day of the week (1=Mon, 7=Sun): ");
  scanf("%d", &start_day);

  for (i = 1; i < start_day; i++) {
    printf("   ");
  }

  for (i = 1; i <= num_days; i++) {
    printf("%3d", i);

    if ((i + start_day) % 7 == 1)
      printf("\n");
  }

  printf("\n");

  return 0;
}
