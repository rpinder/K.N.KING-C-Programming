#include <stdio.h>

int largest_element(int a[], int n);
double average_of_array(int a[], int n);
int number_of_positive(int a[], int n);

int main(void)
{
    int a[5] = {1, 7, -10, 20, 15};

    printf("Largest element in a: %d\n", largest_element(a, 5));
    printf("Average element in a: %.2lf\n", average_of_array(a, 5));
    printf("Positive elements in a: %d\n", number_of_positive(a, 5));
    
    return 0;
}

int largest_element(int a[], int n)
{
    int max = a[0];
    
    for (int i = 1; i < n; i++) {
        if (a[i] > max)
            max = a[i];
    }

    return max;
}

double average_of_array(int a[], int n) {
    int total = 0;

    for (int i = 0; i < n; i++) {
        total += a[i];
    }

    return total / n;
}

int number_of_positive(int a[], int n) {
    int num = 0;

    for (int i = 0; i < n; i++) {
        if (a[i] > 0)
            num++;
    }

    return num;
}
