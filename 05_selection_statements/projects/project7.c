#include <stdio.h>

int main(void)
{
  int max, min, i2, i3, i4;

  printf("Enter four integers: ");
  scanf("%d %d %d %d", &max, &i2, &i3, &i4);

  min = max;

  if (i2 > max) max = i2;
  if (i3 > max) max = i3;
  if (i4 > max) max = i4;

  if (i2 < min) min = i2;
  if (i3 < min) min = i3;
  if (i4 < min) min = i4;
  
  printf("Largest:  %d\n", max);
  printf("Smallest: %d\n", min);

  return 0;
}
