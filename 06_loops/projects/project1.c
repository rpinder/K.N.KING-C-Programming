#include <stdio.h>

int main(void)
{
  float input, max = 0;
  
  do {
    printf("Enter a number: ");
    scanf("%f", &input);

    if (input > max) {
      max = input;
    }
  } while (input > 0.0f);

  printf("The largest number entered was: %.2f\n", max);
  
  return 0;
}
