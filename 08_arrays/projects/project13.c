#include <stdio.h>

int main(void)
{
  char initial, lname[20] = "", ch;
  int i;
  
  printf("Enter a first and last name: ");
  initial = getchar();

  while (getchar() != ' ');
  while ((ch = getchar()) == ' ');
  
  lname[0] = ch;
  
  for (i = 1; (ch = getchar()) != '\n'; i++) {
      lname[i] = ch;
  }

  printf("You entered the Name: ");

  for (i = 0; i < 19 && lname[i] != '\0'; i++) {
          printf("%c", lname[i]);
  }
  
  printf(", %c\n", initial);
  
  return 0;
}
