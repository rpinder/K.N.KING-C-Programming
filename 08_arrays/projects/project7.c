#include <stdio.h>

int main(void)
{
    int array[5][5], i, j, total;

    for (i = 0; i < 5; i++) {
        printf("Enter row %d: ", i + 1);
        for(j = 0; j < 5; j++) {
            scanf("%d", &array[i][j]);
        }
    }

    printf("\nRow totals: ");
    for (i = 0; i < 5; i++) {
        total = 0;
        for (j = 0; j < 5; j++) {
            total += array[i][j];
        }
        printf("%d ", total);
    }

    printf("\nColumn totals: ");
    for (i = 0; i < 5; i++) {
        total = 0;
        for (j = 0; j < 5; j++) {
            total += array[j][i];
        }
        printf("%d ", total);
    }
    
    return 0;
}
