#include <stdio.h>
#include <stdbool.h>

int main(void)
{
    bool weekend[7] = {[0] = 1, [6] = 1};

    for(size_t i = 0; i < sizeof(weekend) / sizeof(weekend[0]); i++) {
        printf("%d\n", weekend[i]);
    }

    return 0;
}
