#include <stdio.h>

int main(void)
{
  int i1, i2, i1c, i2c, rem;

  printf("Enter a fraction: ");
  scanf("%d/%d", &i1, &i2);

  i1c = i1;
  i2c = i2;

  while (i2 != 0) {
    rem = i1 % i2;
    i1 = i2;
    i2 = rem;
  }

  printf("In lowest terms: %d/%d\n", i1c/i1, i2c/i1);

  return 0;
}
