#include <stdio.h>

int fact(int n);

int main(void)
{
    int number = 8;
    printf("%d! = %d\n", number, fact(number));
    return 0;
}

int fact(int n)
{
    return n <= 1 ? 1 : n * fact(n - 1);
}
