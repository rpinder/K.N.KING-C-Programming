#include <stdio.h>

int evaluate_position(char board[8][8]);

int main(void)
{
    char board[8][8] = {{'r',' ',' ',' ','q',' ',' ','k'},
                        {' ',' ',' ',' ',' ','R','b',' '},
                        {' ',' ',' ','p',' ',' ','p',' '},
                        {' ',' ',' ','N',' ',' ',' ','p'},
                        {' ','P',' ','p','P',' ',' ',' '},
                        {' ','n',' ','B',' ',' ',' ','P'},
                        {' ','P',' ',' ',' ','Q','P',' '},
                        {' ','K',' ',' ',' ',' ',' ',' '}};

    printf("Difference: %d\n", evaluate_position(board));
    
    return 0;
}

int evaluate_position(char board[8][8])
{
    int score = 0; 

    for (int x = 0; x < 8; x++) {
        for (int y = 0; y < 8; y++) {
            switch (board[x][y]) {
            case 'Q':
                score += 9; 
                break;
            case 'R':
                score += 5;
                break;
            case 'B':
                score += 3;
                break;
            case 'N':
                score += 3;
                break;
            case 'P':
                score += 1;
                break;
            case 'q':
                score -= 9;
                break;
            case 'r':
                score -= 5;
                break;
            case 'b':
                score -= 3;
                break;
            case 'n':
                score -= 3;
                break;
            case 'p':
                score -= 1;
                break;
            }
        }
    }

    return score;
}
