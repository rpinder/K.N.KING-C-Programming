#include <stdio.h>

int main()
{
    typedef char Int8;
    typedef short int Int16;
    typedef int Int32;

    printf("int8: %zu\n", sizeof(Int8) * 8);
    printf("int16: %zu\n", sizeof(Int16) * 8);
    printf("int32: %zu\n", sizeof(Int32) * 8);

    return 0;
}
