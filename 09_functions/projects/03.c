#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <stdbool.h>

void generate_random_walk(char walk[10][10]);
void print_array(char walk[10][10]);

int main(void)
{
    char array[10][10];

    generate_random_walk(array);
    print_array(array);

    return 0;
}

void generate_random_walk(char walk[10][10])
{
    int position[2] = {0}, i, j, rand_num;
    char step = 'A';
    bool directions[4] = {false}, possible_move;

    srand((unsigned) time(NULL));

    for (i = 0; i < 10; i++) {
        for (j = 0; j < 10; j++) {
            walk[i][j] = '.';
        }
    }

    while (step  <= 'Z') {
        walk[position[0]][position[1]] = step++;

        for (i = 0; i < 4; i++)
            directions[i] = false;

        directions[0] = position[0] != 0 && walk[position[0] - 1][position[1]] == '.';
        directions[1] = position[0] != 9 && walk[position[0] + 1][position[1]] == '.';
        directions[2] = position[1] != 0 && walk[position[0]][position[1] - 1] == '.';
        directions[3] = position[1] != 9 && walk[position[0]][position[1] + 1] == '.';

        possible_move = false;
        for (i = 0; i < 4; i++) {
            if (directions[i]) {
                possible_move = true;
            }
        }

        if (!possible_move)
            break;

        do {
            rand_num = rand() % 4;
        } while (directions[rand_num] != true);

        switch (rand_num) {
        case 0:
            position[0] -= 1;
            break;
        case 1:
            position[0] += 1;
            break;
        case 2:
            position[1] -= 1;
            break;
        case 3:
            position[1] += 1;
            break;
        }
    }
}

void print_array(char walk[10][10])
{
    int i, j;
    
    for (i = 0; i < 10; i++) {
        for (j = 0; j < 10; j++) {
            printf("%c ", walk[i][j]);
        }
        printf("\n");
    }
}
