#include <stdio.h>

float compute_GPA(char grades[], int n);

int main(void)
{
    char grades[] = {'A', 'C', 'D', 'A', 'B'};
    printf("GPA: %.2lf\n", compute_GPA(grades, 5));

    return 0;
}

float compute_GPA(char grades[], int n)
{
    int total = 0;

    for (int i = 0; i < n; i++) {
        switch (grades[i]) {
        case 'A':
            total += 4;
            break;
        case 'B':
            total += 3;
            break;
        case 'C':
            total += 2;
            break;
        case 'D':
            total += 1;
            break;
        }
    }

    return (float) total / n;
}
