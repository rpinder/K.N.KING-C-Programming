#include <stdio.h>

int main(void)
{
    int i1, i2, i3, i4, i5, i6, i7, i8, i9, i10, i11, i12, i13, i14, i15, i16;

    printf("Enter the numbers from 1 to 16 in any order:\n");
    scanf("%d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d",
            &i1, &i2, &i3, &i4, &i5, &i6, &i7, &i8,
            &i9, &i10, &i11, &i12, &i13, &i14, &i15, &i16);

    printf("\n%2d %2d %2d %2d\n", i1, i2, i3, i4);
    printf("%2d %2d %2d %2d\n", i5, i6, i7, i8);
    printf("%2d %2d %2d %2d\n", i9, i10, i11, i12);
    printf("%2d %2d %2d %2d\n\n", i13, i14, i15, i16);

    int row1 = i1  + i2  + i3  + i4;
    int row2 = i5  + i6  + i7  + i8;
    int row3 = i9  + i10 + i11 + i12;
    int row4 = i13 + i14 + i15 + i16;

    int col1 = i1  + i5  + i9  + i13;
    int col2 = i2  + i6  + i10 + i14;
    int col3 = i3  + i7  + i11 + i15;
    int col4 = i4  + i8  + i12 + i16;

    int diag1 = i1 + i6 + i11 + i16;
    int diag2 = i4 + i7 + i10 + i13;

    printf("Row sums:      %d %d %d %d\n", row1, row2, row3, row4);
    printf("Column sums:   %d %d %d %d\n", col1, col2, col3, col4);
    printf("Diagonal sums: %d %d\n", diag1, diag2);

    return 0;
}
