#include <stdio.h>

int fact(int n);

int main(void)
{
    int number = 7;
    printf("%d! = %d\n", number, fact(number));
    return 0;
}

int fact(int n)
{
    int count;
    for (count = 1; n > 1; count *= n--);
    return count;
}
