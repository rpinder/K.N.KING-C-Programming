/* So far the book hasn't said anything about arrays, loops, functions etc so I haven't used them for this code */

#include <stdio.h>

int main(void)
{
    int hours, minutes, total, closest_at, closest_dt, closest_hours, closest_minutes, closest_at_minutes, closest_at_hours, dt[8], at[8];

  dt[0] = 8 * 60 + 0;
  at[0] = 10 * 60 + 16;

  dt[1] = 9 * 60 + 43;
  at[1] = 11 * 60 + 52;

  dt[2] = 11 * 60 + 19;
  at[2] = 13 * 60 + 31;

  dt[3] = 12 * 60 + 47;
  at[3] = 15 * 60;

  dt[4] = 14 * 60;
  at[4] = 16 * 60 + 8;

  dt[5] = 15 * 60 + 45;
  at[5] = 17 * 60 + 55;

  dt[6] = 19 * 60;
  at[6] = 21 * 60 + 20;

  dt[7] = 21 * 60 + 45;
  at[7] = 23 * 60 + 58;

  printf("Enter a 24-hour time: ");
  scanf("%d:%d", &hours, &minutes);

  total = hours * 60 + minutes;

  closest_dt = dt[0];
  closest_at = at[0];

  for (size_t i = 1; i < sizeof(dt) / sizeof(dt[1]); i++) {
      if (((total > dt[i]) ? total - dt[i] : dt[i] - total) < ((total > closest_dt) ? total - closest_dt : closest_dt - total)) {
          closest_dt = dt[i];
          closest_at = at[i];
      }
  }
  
  closest_minutes = closest_dt % 60;
  closest_hours = (closest_dt - closest_minutes) / 60;

  if (closest_hours < 12) {
    printf("Closest departure time is %d:%.2d a.m.", closest_hours, closest_minutes);
  } else if (closest_hours == 12) {
    printf("Closest departure time is %d:%.2d p.m.", closest_hours, closest_minutes);
  } else {
    printf("Closest departure time is %d:%.2d p.m.", closest_hours - 12, closest_minutes);
  }

  closest_at_minutes = closest_at % 60;
  closest_at_hours = (closest_at - closest_at_minutes) / 60;

  if (closest_at_hours < 12) {
    printf(", arriving at %d:%.2d a.m.\n", closest_at_hours, closest_at_minutes);
  } else {
    printf(", arriving at %d:%.2d p.m.\n", closest_at_hours - 12, closest_at_minutes);
  }
  
  return 0;
}
