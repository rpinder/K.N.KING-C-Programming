#include <stdio.h>

int main(void)
{
  int m1, d1, y1, m2, d2, y2, earlier;

  printf("Enter a date (mm/dd/yy): ");
  scanf("%d/%d/%d", &m1, &d1, &y1);

  for (;;) {
    earlier = 0;
    
    printf("Enter a date (mm/dd/yy): ");
    scanf("%d/%d/%d", &m2, &d2, &y2);

    if (m2 == 0 && d2 == 0 && y2 == 0)
      break;

    if (y2 < y1) {
      earlier = 1;
    } else if (y2 == y1){
      if (m2 < m1) {
        earlier = 1;
      } else if (m2 == m1) {
        if (d2 < d1) {
          earlier = 1;
        }
      }
    }

    if (earlier == 1) {
      m1 = m2;
      d1 = d2;
      y1 = y2;
    } 
  }

  printf("%.2d/%.2d/%.2d is the earliest date\n", m1, d1, y1);

  return 0;
}
