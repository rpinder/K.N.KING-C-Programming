#include <stdio.h>
#include <ctype.h>

int main(void)
{
    char message[64] = {0}, ch;
    int msg_length;

    printf("Enter message: ");
    for (msg_length = 0; (ch = getchar()) != '\n'; msg_length++) {
        message[msg_length] = toupper(ch);
    }

    printf("In B1FF-speak: ");
    for (int i = 0; i < msg_length; i++) {
        switch (message[i]) {
            case 'A':
                ch = '4';
                break;
            case 'B':
                ch = '8';
                break;
            case 'E':
                ch = '3';
                break;
            case 'I':
                ch = '1';
                break;
            case 'O':
                ch = '0';
                break;
            case 'S':
                ch = '5';
                break;
            default:
                ch = message[i];
                break;
        }

        putchar(ch);
    }

    printf("!!!!!!!!!!\n");

    return 0;
}
