#include <stdio.h>

int main(void)
{
    int i;

    i = 0;
    while (i < 5) {
        printf("I'm printing\n");
        i++;
        continue;
        printf("I'm not\n");
    }
    i = 0;
    while (i < 5) {
        printf("I'm printing\n");
        goto end;
        printf("I'm not\n");
        end: i++;
    }
}
