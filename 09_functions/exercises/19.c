#include <stdio.h>

void pb(int n);

int main(void)
{
    int number;
    
    printf("Enter a number: ");
    scanf("%d", &number);
    pb(number);
    printf("\n");
    return 0;
}

/* converts int to binary */
void pb(int n)
{
    if (n != 0) {
        pb(n / 2);
        putchar('0' + n % 2);
    }
}
