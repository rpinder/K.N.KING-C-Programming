#include <stdio.h>
#include <ctype.h>

int main(void)
{
  int total = 0;
  char ch;

  printf("Enter a sentence: ");

  while ((ch = getchar()) != '\n') {
    switch (toupper(ch)) {
    case 'A':
    case 'E':
    case 'I':
    case 'O':
    case 'U':
      total++;
      break;
    }
  }

  printf("Your sentence contains %d vowel(s)\n", total);
  
  return 0;
}
