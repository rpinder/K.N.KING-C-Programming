/* Checks numbers for repeated digits */

#include <stdbool.h>
#include <stdio.h>

int main(void)
{
  int digit_seen[10] = {0};
  int digit;
  long n;

  printf("Enter a number: ");
  scanf("%ld", &n);

  while (n > 0) {
    digit = n % 10;
    digit_seen[digit]++;
    n /= 10;
  }

  printf("Digit:      ");
  for (size_t i = 0; i < sizeof(digit_seen) / sizeof(digit_seen[0]); i++) {
    printf("%zu  ", i);
  }

  printf("\nOccurances: ");
  for (size_t i = 0; i < sizeof(digit_seen) / sizeof(digit_seen[0]); i++) {
    printf("%d  ", digit_seen[i]);
  }

  printf("\n");

  return 0;
}
