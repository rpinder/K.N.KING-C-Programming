#include <stdio.h>

int main(void)
{
  int n;
  int m = 20;

  for (n = 0; m > 0; n++, m /= 2)
    printf("%d\n", m);

  return 0;
}
