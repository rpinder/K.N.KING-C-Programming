#include <stdio.h>

int factorial(int num) {
  if (num == 0 || num == 1) {
    return 1;
  } else {
    return (num * factorial(num - 1));
  }
}

int main(void)
{
  int i, n;
  float e = 1.0f;
  
  printf("Enter number of loops: ");
  scanf("%d", &n);

  for (i = 1; i < n; i++) {
    e += 1.0f/factorial(i);
  }

  printf("%f\n", e);
  
  return 0;
}
