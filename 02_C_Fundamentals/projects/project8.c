#include <stdio.h>

int main(void)
{
    float amount_of_loan, interest, payment;

    printf("Enter amount of loan: ");
    scanf("%f", &amount_of_loan);

    printf("Enter interest rate: ");
    scanf("%f", &interest);
    interest = (interest / 12) / 100;

    printf("Enter monthly payment: ");
    scanf("%f", &payment);

    amount_of_loan = amount_of_loan - payment + (amount_of_loan * interest);
    printf("\nBalance remaining after first payment: %.2f\n", amount_of_loan);

    amount_of_loan = amount_of_loan - payment + (amount_of_loan * interest);
    printf("Balance remaining after second payment: %.2f\n", amount_of_loan);

    amount_of_loan = amount_of_loan - payment + (amount_of_loan * interest);
    printf("Balance remaining after first payment: %.2f\n", amount_of_loan);

    return 0;
}
