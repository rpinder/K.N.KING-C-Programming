#include <stdio.h>

#define RADIUS 10
#define PI 3.14159

int main(void)
{
    int radius;

    printf("Enter radius: ");
    scanf("%d", &radius);

    printf("volume: %f\n", (4.0f / 3.0f) * PI * radius * radius * radius);
}
