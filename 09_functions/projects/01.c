#include <stdio.h>

void selection_sort(int arr[], int n);

int main(void)
{
    int numbers[5], i;

    printf("Enter 5 Integers:");

    for (i = 0; i < 5; i++)
        scanf("%d", &numbers[i]);

    selection_sort(numbers, 5);

    printf("Sorted List: ");
    for (i = 0; i < 5; i++)
        printf("%d ", numbers[i]);

    printf("\n");
    return 0;
}

void selection_sort(int arr[], int n)
{
    int largest_index = 0, i, tmp;

    for (i = 1; i < n; i++) {
        if (arr[i] > arr[largest_index]) {
            largest_index = i;
        }
    }

    tmp = arr[n-1];
    arr[n-1] = arr[largest_index];
    arr[largest_index] = tmp;

    if (n > 1) 
        selection_sort(arr, n - 1); 
}


