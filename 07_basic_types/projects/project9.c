#include <stdio.h>
#include <ctype.h>

int main(void)
{
  int hours, minutes;
  char period;

  printf("Enter a 12-hour time: ");
  scanf("%d:%d %c", &hours, &minutes, &period);

  if (toupper(period) == 'P') hours += 12;

  printf("Equivalent 24-hour time: %d:%d\n", hours, minutes);
  
  return 0;
}
