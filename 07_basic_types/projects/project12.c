#include <stdio.h>

int main(void)
{
  double number, total;
  char operator;
  
  printf("Enter an expression: ");
  scanf("%lf", &total);

  while ((operator = getchar()) != '\n') {
    scanf("%lf", &number);

    switch (operator) {
    case '+':
      total += number;
      break;
    case '-':
      total -= number;
      break;
    case '*':
      total *= number;
      break;
    case '/':
      total /= number;
      break;
    }
  }

  printf("%f\n", total);

  return 0;
}
