#include <stdio.h>

int main(void)
{
  int i, n;
  long double total = 1.00f;

  printf("Enter a positive integer: ");
  scanf("%d", &n);

  for (i = n; i > 0; i--) {
    total *= i; 
  }

  printf("Factorial of %d: %Lf\n", n, total);
  return 0;
}
