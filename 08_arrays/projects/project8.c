#include <stdio.h>

int main(void)
{
    int array[5][5], i, j, total, high, low;

    for (i = 0; i < 5; i++) {
        printf("Enter student %d's grades: ", i + 1);
        for(j = 0; j < 5; j++) {
            scanf("%d", &array[i][j]);
        }
    }

    printf("\n");
    for (i = 0; i < 5; i++) {
        total = 0;
        for (j = 0; j < 5; j++) {
            total += array[i][j];
        }
        printf("Student %i: %d %.2f\n", i + 1
               , total, (float) total/5);
    }

    printf("\n");
    for (i = 0; i < 5; i++) {
        total = 0;
        high = 0;
        low = 100;
        for (j = 0; j < 5; j++) {
            total += array[j][i];
            if (array[j][i] > high) {
                high = array[j][i];
            }
            if (array[j][i] < low) {
                low = array[j][i];
            }
        }
        printf("Quiz %d: %d %.2f %d \n", i + 1, high, (float) total/5, low);
    }
    
    return 0;
}
