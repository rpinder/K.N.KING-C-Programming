#include <stdio.h>

int check(int x, int y, int n);

int main(void)
{
    printf("%d\n", check(5, 2, 10));
    printf("%d\n", check(-5, -2, -10));
    printf("%d\n", check(-2, 5, 3));
    printf("%d\n", check(0, 6, 7));
    
    return 0;
}

int check(int x, int y, int n)
{
    if (n - 1 > 0) {
        if ((x >= 0 && x <= n - 1) && (y >= 0 && y <= n - 1)) {
            return 1;
        }
    } else {
        if ((x <= 0 && x >= n - 1) && (y <= 0 && y >= n - 1)) {
            return 1;
        }
    }

    return 0;
}
