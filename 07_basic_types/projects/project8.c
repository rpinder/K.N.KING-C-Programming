/* So far the book hasn't said anything about arrays, loops, functions etc so I haven't used them for this code */

#include <stdio.h>
#include <ctype.h>

int main(void)
{
  int hours, minutes, total, closest_at, closest_dt, closest_hours, closest_minutes, closest_at_minutes, closest_at_hours,
    dt1, at1,
    dt2, at2,
    dt3, at3,
    dt4, at4,
    dt5, at5,
    dt6, at6,
    dt7, at7,
    dt8, at8;
  char period;

  dt1 = 8 * 60 + 0;
  at1 = 10 * 60 + 16;

  dt2 = 9 * 60 + 43;
  at2 = 11 * 60 + 52;

  dt3 = 11 * 60 + 19;
  at3 = 13 * 60 + 31;

  dt4 = 12 * 60 + 47;
  at4 = 15 * 60;

  dt5 = 14 * 60;
  at5 = 16 * 60 + 8;

  dt6 = 15 * 60 + 45;
  at6 = 17 * 60 + 55;

  dt7 = 19 * 60;
  at7 = 21 * 60 + 20;

  dt8 = 21 * 60 + 45;
  at8 = 23 * 60 + 58;

  printf("Enter a 12-hour time (h:m am/pm): ");
  scanf("%d:%d %c", &hours, &minutes, &period);

  if (toupper(period) == 'P') {
    hours+=12;
  }

  total = hours * 60 + minutes;

  closest_dt = dt1;
  closest_at = at1;
 
  if (((total > dt2) ? total - dt2 : dt2 - total) < ((total > closest_dt) ? total - closest_dt : closest_dt - total)) {
    closest_dt = dt2;
    closest_at = at2;
  }
  
  if (((total > dt3) ? total - dt3 : dt3 - total) < ((total > closest_dt) ? total - closest_dt : closest_dt - total)) {
    closest_dt = dt3;
    closest_at = at3;
  }
  

  if (((total > dt4) ? total - dt4 : dt4 - total) < ((total > closest_dt) ? total - closest_dt : closest_dt - total)) {
    closest_dt = dt4;
    closest_at = at4;
  }

  if (((total > dt5) ? total - dt5 : dt5 - total) < ((total > closest_dt) ? total - closest_dt : closest_dt - total)) {
    closest_dt = dt5;
    closest_at = at5;
  }

  if (((total > dt6) ? total - dt6 : dt6 - total) < ((total > closest_dt) ? total - closest_dt : closest_dt - total)) {
    closest_dt = dt6;
    closest_at = at6;
  }

  if (((total > dt7) ? total - dt7 : dt7 - total) < ((total > closest_dt) ? total - closest_dt : closest_dt - total)) {
    closest_dt = dt7;
    closest_at = at7;
  }

  if (((total > dt8) ? total - dt8 : dt8 - total) < ((total > closest_dt) ? total - closest_dt : closest_dt - total)) {
    closest_dt = dt8;
    closest_at = at8;
  }

  closest_minutes = closest_dt % 60;
  closest_hours = (closest_dt - closest_minutes) / 60;

  if (closest_hours < 12) {
    printf("Closest departure time is %d:%.2d a.m.", closest_hours, closest_minutes);
  } else if (closest_hours == 12) {
    printf("Closest departure time is %d:%.2d p.m.", closest_hours, closest_minutes);
  } else {
    printf("Closest departure time is %d:%.2d p.m.", closest_hours - 12, closest_minutes);
  }

  closest_at_minutes = closest_at % 60;
  closest_at_hours = (closest_at - closest_at_minutes) / 60;

  if (closest_at_hours < 12) {
    printf(", arriving at %d:%.2d a.m.\n", closest_at_hours, closest_at_minutes);
  } else {
    printf(", arriving at %d:%.2d p.m.\n", closest_at_hours - 12, closest_at_minutes);
  }
  
  return 0;
}
