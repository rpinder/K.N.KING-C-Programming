/* Prints a table of squares using a for statement */

/*

smallest value of n that causes failure

short = 181
int   = 46340
long  = Theoretically 3 037 000 499

*/

#include <stdio.h>

int main(void)
{
  long i, n;

  printf("This program prints a table of squares.\n");
  printf("Enter number of entries in table: ");
  scanf("%ld", &n);

  for (i = 1; i <= n; i++)
    printf("%20ld%20ld\n", i, i * i);

  return 0;
}
