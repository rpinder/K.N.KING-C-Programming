#include <stdio.h>

double inner_product(double a[], double b[], int n);

int main(void)
{
    double a[] = {1.00, 2.54, 3.14, 5.95};
    double b[] = {5.21, 7.74, 1.12, 4.80};

    printf("Inner Product: %.4lf\n", inner_product(a, b, 4));
}

double inner_product(double a[], double b[], int n)
{
    double total = 0;

    for (int i = 0; i < n; i++) {
        total += a[i] * b[i];
    }

    return total;
}
