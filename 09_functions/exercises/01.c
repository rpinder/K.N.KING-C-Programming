double triangle_area(double base, height)
double product;
{
    product = base * height;
    return product / 2;
}

// Errors:
// 'double product' should be included inside the function body
// height isn't given a type

/* Corrected */

double corrected_triangle_area(double base, double height)
{
    double product;

    product = base * height;
    return product / 2;
}

