#include <stdio.h>

int main(void)
{
    char sentence[50] = "", punctuation, ch;
    int j, i, num_of_words = 1, word_length, current_index;

    printf("Enter a sentence: ");
    
    for (i = 0; (ch = getchar()) != '\n' && ch != '?' && ch != '.' && ch != '!' && ch != 0; i++) {
        sentence[i] = ch;
        if (ch == ' ')
            num_of_words++;
    }

    punctuation = ch;

    printf("Reversal of sentence: ");

    for (current_index = 0; sentence[current_index] != 0; current_index++);

    for (j = 0; j < num_of_words; j++) {
        if (j + 1 < num_of_words) {
            for (word_length = 0; (ch = sentence[current_index - word_length - 1]) != ' '; word_length++);
        }
    
        for (i = current_index - word_length; i < current_index; i++) {
            printf("%c", sentence[i]);
        }

        if (j != num_of_words - 1)
            putchar(' ');

        current_index -= word_length + 1;
    }

    printf("%c\n", punctuation);

    return 0;
}
