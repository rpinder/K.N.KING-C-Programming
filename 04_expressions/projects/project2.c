#include <stdio.h>

int main(void)
{
  int number;
  int first_digit;
  int second_digit;
  int third_digit;
  
  printf("Enter a three digit number: ");
  scanf("%d", &number);

  third_digit = number % 10;
  second_digit = number / 10 % 10;
  first_digit = number / 100;

  printf("The reversal is: %d%d%d\n", third_digit, second_digit, first_digit);

  return 0;
}
