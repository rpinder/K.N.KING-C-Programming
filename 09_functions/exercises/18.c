#include <stdio.h>

int gcd(int m, int n);

int main(void)
{
    int m, n;

    m = 9;
    n = 12;

    printf("%d and %d: %d\n", m, n, gcd(m, n));

    return 0;
}

int gcd(int m, int n)
{
    return n == 0 ? m : gcd(n, m % n);
}
