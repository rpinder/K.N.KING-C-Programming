#include <stdio.h>

int main(void)
{
  char initial, lname, ch;
  
  printf("Enter a first and last name: ");
  initial = getchar();

  while (getchar() != ' ');
  while ((ch = getchar()) == ' ');

  putchar(ch);

  while ((lname = getchar()) != '\n') {
    putchar(lname);
  }

  printf(" , %c.\n", initial);
  
  return 0;
}
