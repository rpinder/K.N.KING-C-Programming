#include <stdio.h>

int main(void)
{
  printf("%zu\n%zu\n%zu\n%zu\n%zu\n%zu\n",
         sizeof(int),
         sizeof(short),
         sizeof(long),
         sizeof(float),
         sizeof(double),
         sizeof(long double));
  
  return 0;
}
